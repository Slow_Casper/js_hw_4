let num1, num2, operator

while (!num1 || num1 == NaN || !num2 || num2 == NaN) {
    num1 = +prompt ("Введіть перше число", num1);
    num2 = +prompt ("Введіть друге число", num2);
}

while (operator !== "+" && operator !== "-" && operator !== "*" && operator !== "/") {
    operator = prompt ("Введіть математичний оператор", operator);
}

function operation (num1, num2, operator) {
    let result;
        
    if (operator === "+") {
        result = num1 + num2;
    }
    if (operator === "-") {
        result = num1 - num2;
    }
    if (operator === "*") {
        result = num1 * num2;
    }
    if (operator === "/") {
        result = num1 / num2;
    }

    console.log(result);
}


operation (num1, num2, operator);


// num1 = +prompt ("Введіть перше число", num1);
// num2 = +prompt ("Введіть друге число", num2);
// operator = prompt ("Введіть математичний оператор", operator);

// console.log(num1, typeof num1);
// console.log(num2, typeof num2);
// console.log(operator, typeof operator);
// console.log(num1, operator, num2);
